function update_screen(field, value)
{
    var display = document.getElementById(field);
    display.innerHTML = value;
}

function generate()
{
    var first = Math.ceil(Math.random() * 100);
    var second = Math.ceil(Math.random() * 100);
    var operator = "+";

    update_screen('first', first);
    update_screen('operator', operator);
    update_screen('second', second);
}

function answer()
{
    var message = document.getElementById('message');

    var first = document.getElementById('first').innerHTML;
    var operator = document.getElememtById('operator').innerHTML;
    var second = document.getElementById('second').innerHTML;
    var answer = document.getElememtById('result').value;

    message.innerHTML = answer;

    /*
    var result = eval(first + operator + second);

    if (Math.round(result) == Math.round(answer))
    {
        message.innerHTML = 'Correct.';
    } else {
        message.innerHTML = 'Wrong.';
    } */
    generate();
}

function build_screen()
{
    var body = document.getElementById("dom_handle");
    body.innerHTML = "<div id='main'>" +
      "<div id='message'></div>" +
      "<div id='question'>" +
        "<form action='javascript:void(0);' onsubmit='answer();'>" +
          "<span id='first'></span>" +
          "<span id='operator'></span>" +
          "<span id='second'></span>" +
          "=" + 
          "<input type='text' name='result' id='result'>" +
        "</form>" +
      "</div>" +
    "</div>";
}

build_screen();
generate();
